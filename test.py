"""Test the project's API."""


# [ Imports ]
import example

# [ Test ]
assert example.create_first_greeting() == "Hello caller 1.  I'm the first example!"
assert example.create_first_greeting() == "Hello caller 2.  I'm the first example!"
assert example.create_second_greeting() == "Hello caller 3.  I'm the second example!"
assert example.create_second_greeting() == "Hello caller 4.  I'm the second example!"
