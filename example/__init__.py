"""Example Project API."""


# [ Imports ]
# from . import core
from .core.first import create_greeting as create_first_greeting
from .core.second import create_greeting as create_second_greeting
