"""Project script/API."""


# [ Imports ]
# [ -Python ]
import sys
# [ -Project ]
import example


# [ Internal ]
def _first():
    """Main entry point for first module."""
    greeting = example.create_first_greeting()
    print(greeting)


def _second():
    """Main entry point for second module."""
    greeting = example.create_second_greeting()
    print(greeting)


# [ API ]
def main():
    """Main entry point for first or second or help."""
    command = sys.argv[1] if len(sys.argv) >= 2 else "help"

    if command == 'help':
        print('valid commands are "first" or "second".')
    elif command == 'first':
        _first()
    elif command == 'second':
        _second()
    else:
        print(f'Invalid command {command}.  try "help".')
        exit(1)
