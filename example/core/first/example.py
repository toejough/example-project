"""Project script/API."""


# [ Imports ]
from example.core import util


# [ API ]
def create_greeting():
    """Create a greeting."""
    caller_number = util.increment_caller()
    return f"Hello caller {caller_number}.  I'm the first example!"
