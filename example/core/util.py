"""Example utility."""


# [ API ]
def increment_caller():
    """Increment and return the caller number."""
    global _CALLER_NUMBER
    _CALLER_NUMBER += 1
    return _CALLER_NUMBER


# [ Internal ]
_CALLER_NUMBER = 0
