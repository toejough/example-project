"""Setup script for the example package."""


# [ Imports ]
# [ -Python ]
from setuptools import setup, find_packages
# [ -Project ]
import example

# The setup function is where you specify your project's attributes
setup(
    # package name
    name='example',
    # package version
    version='0.1.0',
    # short description
    description='An example package',
    # not strictly necessary, but it's a good practice to provide a url if you have one (and you should)
    url='https://github.com/notion/example',
    # you.  you want attribution, don't you?
    author='joe',
    # your email.  Or, *an* email.  If you supply an 'author', pypi requires you supply an email.
    author_email='joe@notion.ai',
    # a license
    license='MIT',
    # "classifiers", for reasons.  Below is adapted from the official docs at https://packaging.python.org/en/latest/distributing.html#classifiers
    classifiers=[
        # How mature is this project? Common values are
        #   3 - Alpha
        #   4 - Beta
        #   5 - Production/Stable
        'Development Status :: 3 - Alpha',

        # Indicate who your project is intended for
        'Intended Audience :: Developers',

        # Pick your license as you wish (should match "license" above)
            'License :: OSI Approved :: MIT License',

        # Specify the Python versions you support here. In particular, ensure
        # that you indicate whether you support Python 2, Python 3 or both.
        'Programming Language :: Python :: 3',
        'Programming Language :: Python :: 3.6',
    ],
    # keywords.  because classifiers are for serious metadata only?
    keywords="example project",
    # what packages are included.  'find_packages' will automatically find your packages, but you can list them manually ('hello_world') if you want.
    packages=find_packages(),
    # minimum requirements for installation (third-party packages your package uses)
    install_requires=[],
    # extras (installed like "pip install example[web]")
    extras_require={
        'static-checks': ['vulture', 'prospector', 'pylint==1.6.5'],
        'web': ['flask'],
        'training': ['awesome-data-source-package']
    },
    # give your package an executable.
    entry_points={
        'console_scripts': [
            # <name>=<package>:<function>
            # when the user calls 'hello_world' on the cli, 'hello_world/__init__.py::_cli()" will be called.
            # 'hello_world=hello_world:_cli',
            "example-cli=example.cli:main",
        ],
    }
)
