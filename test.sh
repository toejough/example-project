#! /usr/bin/env bash


# call the example project
clear
echo "installing..."
pip install -I -e .
echo

echo "testing API call..."
python test.py && echo "test script passed" || echo "test script failed!"
echo

echo "testing binaries..."
example-cli help
echo

echo "testing zipapp..."
python -m zipapp -p "/usr/bin/env python3.6" -o example-app example -m "example.cli:main"
./example-app first
./example-app second
