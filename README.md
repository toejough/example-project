Example Project ideas and rationale

# Structure
```
project_folder
├── project
│   ├── __ini__.py
│   ├── cli.py
│   ├── web.py
│   ├── vr.py
│   ├── ui_util.py
│   └── core
│       ├── __ini__.py
│       ├── logic_module_1
│       │   ├── __init__.py
│       │   └── example.py
│       ├── logic_module_2
│       │   ├── __init__.py
│       │   └── example.py
│       └── util.py
└── setup.py
```

## Project Folder
The project folder contains:

* the project
* a `setup.py`

The poject is the project - it's where all the code for the project goes.

The `setup.py` file is what python uses to install your project so that it is importable with `import project` and,
if configured, runnable as a CLI command `>: project`.

### Project code
The project code is organized into top-level modules that roughly correspond to user-interfaces:

* `__init__.py` - exposes the programmatic API you wish users to be able to access when they import the project.
* `cli.py` - cli-specific logic
* `web.py` - web-sepecific logic
* `vr.py` - vr-headset-specific logic
* `ui_util.py` - common UI utilities
* `core.py` - core application logic, which is used by the UI modules and `__init__.py`

#### Core/submodules
Core, or any of the other modules that ends up turning into a mult-file module, gets organized similarly, with
inits, independent submodules, and possibly some common utilities.

* `__init__.py` - exposes the programmatic API you wish callers of this module to be able to access when they import
  this module.
* `logic_module_1` - a submodule containing some (at least mostly)-independent logic
* `logic_module_2` - a submodule containing some (at least mostly)-independent logic
* `util.py` - common utilities for the other submodules at this level

## Setup.py
Lots of stuff in here, and the example `setup.py` is pretty well commented, but I'll take some extra time to call out:
* project name - keep this up-to-date with your file structure (no logic impact here, just sensible).
* version - keep this up-to-date across all the places you use/display versions (like version control, README's, code, etc)
* entry-points - binaries generated for your project when you install it!
